const CubeLeft = echarts.graphic.extendShape({
    shape: {},
    buildPath: function (ctx, shape) {
        ctx.moveTo(shape.c1[0], shape.c1[1]).lineTo(shape.c2[0], shape.c2[1]).lineTo(shape.c3[0], shape.c3[1]).lineTo(shape.c4[0], shape.c4[1]).closePath()
    }
})
const CubeRight = echarts.graphic.extendShape({
    shape: {},
    buildPath: function (ctx, shape) {
        ctx.moveTo(shape.c1[0], shape.c1[1]).lineTo(shape.c2[0], shape.c2[1]).lineTo(shape.c3[0], shape.c3[1]).lineTo(shape.c4[0], shape.c4[1]).closePath()
    }
})
const CubeTop = echarts.graphic.extendShape({
    shape: {},
    buildPath: function (ctx, shape) {
        ctx.moveTo(shape.c1[0], shape.c1[1]).lineTo(shape.c2[0], shape.c2[1]).lineTo(shape.c3[0], shape.c3[1]).lineTo(shape.c4[0], shape.c4[1]).closePath()
    }
})
echarts.graphic.registerShape('CubeLeft', CubeLeft)
echarts.graphic.registerShape('CubeRight', CubeRight)
echarts.graphic.registerShape('CubeTop', CubeTop)

function initRender(setting) {
    var color = setting.color
    var width = setting.param1
    var height = setting.param2
    var option = setting.option

    //构建占比阴影部分
    if (setting.showAll) {
        option.series[0].renderItem = (params, api) => {
            const location = api.coord([api.value(0), api.value(1)])
            const yAxisPoint = api.coord([0, api.value(1)])
            return {
                type: 'group',
                children: [{
                    type: 'CubeLeft',
                    shape: {
                        api,
                        c1: [location[0], yAxisPoint[1]],
                        c2: [location[0] + width, yAxisPoint[1] - height],
                        c3: [yAxisPoint[0] + width, yAxisPoint[1] - height],
                        c4: [yAxisPoint[0], yAxisPoint[1]]
                    },
                    style: {
                        fill: setting.shadowColor,
                    },
                    silent: true,
                }, {
                    type: 'CubeRight',
                    shape: {
                        api,
                        c1: [location[0], yAxisPoint[1]],
                        c2: [yAxisPoint[0], yAxisPoint[1]],
                        c3: [yAxisPoint[0], yAxisPoint[1] + height],
                        c4: [location[0], yAxisPoint[1] + height]
                    },
                    style: {
                        fill: setting.shadowColor
                    },
                    silent: true,
                }, {
                    type: 'CubeTop',
                    shape: {
                        api,
                        c1: [location[0], yAxisPoint[1]],
                        c2: [location[0] + width, yAxisPoint[1] - height],
                        c3: [location[0] + width, yAxisPoint[1]],
                        c4: [location[0], yAxisPoint[1] + height]
                    },
                    style: {
                        fill: setting.shadowColor
                    },
                    silent: true,
                }],
            }
        }
    }

    option.series[1].renderItem = function (params, api) {
        const location = api.coord([api.value(0), 0])
        const yAxisPoint = api.coord([0, api.value(1)])
        return {
            type: 'group',
            children: [{
                type: 'CubeLeft',
                shape: {
                    api,
                    c1: [location[0], yAxisPoint[1]],
                    c2: [location[0] + width, yAxisPoint[1] - height],
                    c3: [yAxisPoint[0] + width, yAxisPoint[1] - height],
                    c4: [yAxisPoint[0], yAxisPoint[1]]
                },
                style: {
                    fill: new echarts.graphic.LinearGradient(1, 0, 0, 0, [{
                        offset: 0,
                        color: color[1]
                    },
                    {
                        offset: 1,
                        color: color[0]
                    }
                    ]),
                }
            }, {
                type: 'CubeRight',
                shape: {
                    api,
                    c1: [location[0], yAxisPoint[1]],
                    c2: [yAxisPoint[0], yAxisPoint[1]],
                    c3: [yAxisPoint[0], yAxisPoint[1] + height],
                    c4: [location[0], yAxisPoint[1] + height]
                },
                style: {
                    fill: new echarts.graphic.LinearGradient(1, 0, 0, 0, [{
                        offset: 0,
                        color: color[1]
                    },
                    {
                        offset: 1,
                        color: color[0]
                    }
                    ]),
                }
            }, {
                type: 'CubeTop',
                shape: {
                    api,
                    c1: [location[0], yAxisPoint[1]],
                    c2: [location[0] + width, yAxisPoint[1] - height],
                    c3: [location[0] + width, yAxisPoint[1]],
                    c4: [location[0], yAxisPoint[1] + height]
                },
                style: {
                    fill: new echarts.graphic.LinearGradient(1, 0, 0, 0, [{
                        offset: 0,
                        color: color[1]
                    },
                    {
                        offset: 1,
                        color: color[1]
                    }
                    ]),
                }
            }],
        }
    }
}