import request from '@/utils/request'

// 获取用户详细信息
export function ssoLogin() {
    return request({
      url: '/ssoLogin',
      method: 'get'
    })
  }